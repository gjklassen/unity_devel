using UnityEngine;
using System.Collections;

public class Vision : MonoBehaviour {
	
	private float horizontalSpeed = 1.0F;
    private float verticalSpeed = 1.0F;
	
	void Start () {
		transform.position = BeLight.cameraVector;
	}
	
	void Update () {
		float h = horizontalSpeed * Input.GetAxis("Mouse X");
        float v = verticalSpeed * Input.GetAxis("Mouse Y");
		//transform.Rotate (new Vector3(-v,-h,0.0f));
		//transform.position = BeLight.cameraVector;
		if(Input.GetKeyDown(KeyCode.LeftArrow)){
			if (BeLight.selX > 0){
				BeLight.selX = BeLight.selX - 1;
			}
		}
		if(Input.GetKeyDown(KeyCode.RightArrow)){
			if (BeLight.selX < BeLight.MATRIX_N-1){
				BeLight.selX = BeLight.selX + 1;
			}
		}
		if(Input.GetKeyDown (KeyCode.UpArrow)){
			if (BeLight.selY < BeLight.MATRIX_N-1){
				BeLight.selY = BeLight.selY + 1;
			}
		}
		if (Input.GetKeyDown (KeyCode.DownArrow)){
			if (BeLight.selY > 0){
				BeLight.selY = BeLight.selY - 1;
			}
		}
		if (Input.GetKeyDown(KeyCode.Space)){
			if (BeLight.isSel == false){
				BeLight.sX = BeLight.selX;
				BeLight.sY = BeLight.selY;
				BeLight.isSel = true;
			}else{
				BeLight.fX = BeLight.selX;
				BeLight.fY = BeLight.selY;
				BeLight.swapReady = true;
				BeLight.isSel = false;
			}
		}
 		if(Input.GetMouseButtonDown(0)){
			BeLight.clicked = true;
			BeLight.mouseDown = transform.position;
			Debug.Log(BeLight.mouseDown);
		}
		if(Input.GetMouseButtonUp(0)){
			BeLight.clicked = false;
			BeLight.mouseUp = transform.position;
		}
	}
}
