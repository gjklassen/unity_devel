using UnityEngine;
using System.Collections;

public class BeLight : MonoBehaviour {
	
	public static int MATRIX_N = 4;
	public static Quaternion QID = Quaternion.identity;
	public static float MATRIX_PLANE = 0.0F;
	public static Vector3 cameraVector = new Vector3(1.5F,1.5F,-5.0F);
	public static Vector3 cursorVector = new Vector3(0.0f,0.0f,-1.0f);
	public Transform cubePrefab = null;
	public Transform[,] matrix = null;
	public Material matTrue = null;
	public Material matChosen = null;
	public bool[,] matrixValues = null;
	public static bool clicked = false;
	public static Vector3 mouseDown, mouseUp;
	public static int selX = 0;
	public static int selY = 0;
	public static bool isSel = false;
	public static bool swapReady = false;
	public static int[,] subOrders;
	public static int sX = 0;
	public static int sY = 0;
	public static int fX = 0;
	public static int fY = 0;
	
	private Transform[,] CreateMatrix(int dimension){
		Transform[,] cubes = new Transform[dimension,dimension];
		for(int i=0; i < dimension; i++){
			for(int j=0; j < dimension; j++){
				Vector3 cubeVector = new Vector3(1 * j,i * 1,0.0f);
				Transform cube = Instantiate(cubePrefab, cubeVector, QID) as Transform;
				string cubeName = i.ToString() + "," + j.ToString();
				cube.renderer.material = matTrue;
				cube.name = cubeName;
				//cube.Rotate(new Vector3(0,0,0));
				cubes[i,j] = cube;
			}
		}
		
		return cubes;
	}
	
	void SetColors(){
		for(int i=0; i < MATRIX_N; i++){
			for(int j=0; j < MATRIX_N; j++){
				if(j == selX && i == selY){
					//matrix[i,j].renderer.material = matChosen;
					matrix[i,j].renderer.material = matTrue;
					matrix[i,j].renderer.material.color = Color.yellow;
				}else{
					matrix[i,j].renderer.material = matTrue;	
				if (matrixValues[i,j]){
					matrix[i,j].renderer.material.color = Color.blue;
				}else{
					matrix[i,j].renderer.material.color = Color.magenta;
				}
				}
			}
		}
	}
	
	void SwapRowCol(int start, int finish, string rowcol){
		bool[,] tmatrix = new bool[MATRIX_N,MATRIX_N];
		
		for(int i=0; i < MATRIX_N; i++){
			for(int j=0; j < MATRIX_N; j++){
				tmatrix[i,j] = matrixValues[i,j];
			}
		}
		for(int n=0; n < MATRIX_N; n++){
			if(rowcol == "row"){
				matrixValues[finish,n] = tmatrix[start,n];
				matrixValues[start,n] = tmatrix[finish,n];
			}else{
				matrixValues[n,finish] = tmatrix[n,start];
				matrixValues[n,start] = tmatrix[n,finish];
			}
		}
	}
	
	void Get2by2s(){
		int xct = 0;
		int yct = 0;
		for(int i=0; i < MATRIX_N - 1; i = i + 2){
			for(int j=0; j < MATRIX_N - 1; j = j + 2){
				int count = 0;
				if (matrixValues[i,j] == true){
						count = count + 1;
				}
				if (matrixValues[i+1,j] == true){
						count = count + 1;
				}
				if (matrixValues[i,j+1] == true){
						count = count + 1;
				}
				if (matrixValues[i+1,j+1] == true){
						count = count + 1;
				}
				if (count == 2 || count == 4){
					subOrders[xct,yct] = 0;
						matrix[i,j].position = new Vector3(matrix[i,j].position.x,matrix[i,j].position.y,2.0f);
						matrix[i+1,j].position = new Vector3(matrix[i+1,j].position.x,matrix[i+1,j].position.y,2.0f);
						matrix[i,j+1].position = new Vector3(matrix[i,j+1].position.x,matrix[i,j+1].position.y,2.0f);
						matrix[i+1,j+1].position = new Vector3(matrix[i+1,j+1].position.x,matrix[i+1,j+1].position.y,2.0f);
				}else{
					subOrders[xct,yct] = 1;
					matrix[i,j].position = new Vector3(matrix[i,j].position.x,matrix[i,j].position.y,0.0f);
					matrix[i+1,j].position = new Vector3(matrix[i+1,j].position.x,matrix[i+1,j].position.y,0.0f);
					matrix[i,j+1].position = new Vector3(matrix[i,j+1].position.x,matrix[i,j+1].position.y,0.0f);
					matrix[i+1,j+1].position = new Vector3(matrix[i+1,j+1].position.x,matrix[i+1,j+1].position.y,0.0f);

				}
				yct++;
			}
			yct = 0;
			xct++;
		}
		xct = 0;
	}
	
	public void initMatrix(){
		matrixValues[0,0] = true;
		matrixValues[0,1] = false;
		matrixValues[0,2] = false;
		matrixValues[0,3] = true;
		matrixValues[1,0] = true;
		matrixValues[1,1] = true;
		matrixValues[1,2] = false;
		matrixValues[1,3] = false;
		matrixValues[2,0] = true;
		matrixValues[2,1] = false;
		matrixValues[2,2] = true;
		matrixValues[2,3] = false;
		matrixValues[3,0] = true;
		matrixValues[3,1] = true;
		matrixValues[3,2] = true;
		matrixValues[3,3] = true;
	}
	
	void Start () {
		float midpoint = (MATRIX_N / 2) - 0.5f;
		matrix = CreateMatrix(MATRIX_N);
		matrixValues = new bool[MATRIX_N,MATRIX_N];
		subOrders = new int[MATRIX_N/2,MATRIX_N/2];
		cameraVector = new Vector3(midpoint,midpoint,-MATRIX_N - 1);
		cursorVector = new Vector3(midpoint,midpoint,-2.0f);
		selX = 0;
		selY = 0;
		
		initMatrix();
	}
	
	void Update () {
		if(swapReady){
			SwapRowCol(sY,fY,"row");
			SwapRowCol(sX,fX,"col");
			swapReady = false;
		}
		if (Input.GetKeyDown (KeyCode.R)){
			initMatrix();
		}
		SetColors();
		Get2by2s();
	}
}
