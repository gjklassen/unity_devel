using UnityEngine;
using System.Collections;

public class CursorActivity : MonoBehaviour {
	
	void Start() {
		transform.position = BeLight.cursorVector;
	}
	

    void Update() {
		if(Input.GetKeyDown(KeyCode.LeftArrow)){
			if (BeLight.selX > 0){
				BeLight.selX = BeLight.selX - 1;
			}
			Debug.Log (BeLight.selX + " " + BeLight.selY);
		}
		if(Input.GetKeyDown(KeyCode.RightArrow)){
			if (BeLight.selX < BeLight.MATRIX_N-1){
				BeLight.selX = BeLight.selX + 1;
			}
			Debug.Log (BeLight.selX + " " + BeLight.selY);
		}
		if(Input.GetKeyDown (KeyCode.UpArrow)){
			if (BeLight.selY < BeLight.MATRIX_N-1){
				BeLight.selY = BeLight.selY + 1;
			}
			Debug.Log (BeLight.selX + " " + BeLight.selY);
		}
		if (Input.GetKeyDown (KeyCode.DownArrow)){
			if (BeLight.selY > 0){
				BeLight.selY = BeLight.selY - 1;
			}
		}
 		if(Input.GetMouseButtonDown(0)){
			BeLight.clicked = true;
			BeLight.mouseDown = transform.position;
			Debug.Log(BeLight.mouseDown);
		}
		if(Input.GetMouseButtonUp(0)){
			BeLight.clicked = false;
			BeLight.mouseUp = transform.position;
		}
	}
}
