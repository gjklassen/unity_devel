# README
# This is the readme file for the UManitoba Mathematics rtl_devel repository.
# Use this repository for VHDL and other FPGA related languages.
# To compile and use these resources you will require the Unity engine.
# This can be found at unity3d.com
# The free edition is fully featured and works fine; it works under Win/OSX/*nix.
# Repo Location: bitbucket.org/gjklassen/unity_devel
